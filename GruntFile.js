module.exports = function(grunt){

// Configuration
grunt.initConfig({
concat: {
    js:{
        src: ['wp/wp-content/themes/radougadistilleries/js/libs/jquery-1.7.1.min.js',
        'wp/wp-content/themes/radougadistilleries/js/plugins.js',
        'wp/wp-content/themes/radougadistilleries/js/script.js', 'wp/wp-content/themes/radougadistilleries/js/libs/modernizr-2.6.2.min.js','wp/wp-content/themes/radougadistilleries/rs-plugin/js/jquery.themepunch.tools.min.js','wp/wp-content/themes/radougadistilleries/rs-plugin/js/jquery.themepunch.revolution.min.js','wp/wp-content/themes/radougadistilleries/rs-plugin/js/extensions/revolution.extension.actions.min.js',
        'wp/wp-content/themes/radougadistilleries/rs-plugin/js/extensions/revolution.extension.carousel.min.js',
        'wp/wp-content/themes/radougadistilleries/rs-plugin/js/extensions/revolution.extension.kenburn.min.js',
        'wp/wp-content/themes/radougadistilleries/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js',
        'wp/wp-content/themes/radougadistilleries/rs-plugin/js/extensions/revolution.extension.migration.min.js',
        'wp/wp-content/themes/radougadistilleries/rs-plugin/js/extensions/revolution.extension.navigation.min.js',
        'wp/wp-content/themes/radougadistilleries/rs-plugin/js/extensions/revolution.extension.parallax.min.js',
        'wp/wp-content/themes/radougadistilleries/rs-plugin/js/extensions/revolution.extension.slideanims.min.js',
        'wp/wp-content/themes/radougadistilleries/rs-plugin/js/extensions/revolution.extension.video.min.js'],
        dest:'wp/wp-content/themes/radougadistilleries/js/build/scripts.js'
    }
},
uglify:{
    build:{
        files:[{
            src:'wp/wp-content/themes/radougadistilleries/js/build/scripts.js',
            dest: 'wp/wp-content/themes/radougadistilleries/js/build/scripts.js'
        }]
    }
}

});

// Load Plugins
grunt.loadNpmTasks('grunt-contrib-concat');
grunt.loadNpmTasks('grunt-contrib-uglify');

}