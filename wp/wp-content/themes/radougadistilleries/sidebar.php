<div id="sidebar">
				<div class="sidebar-item widget">
					<h6 class="widget-title">Recent Posts</h6>
						<ul>
							<?php
                $recent_posts = wp_get_recent_posts();
                foreach( $recent_posts as $recent ){
                    echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';
                }
                ?>
						</ul>
				<div class="sidebar-divider"></div>
				</div>
				<div id="categories-2" class="sidebar-item widget widget_categories"><h6 class="widget-title">Categories</h6>
					<ul>
                    <?php wp_list_categories( array(
        'orderby'  => 'count' ,
        'title_li' => '',
		'order' => 'desc',
		'show_count' => 1,
		'current_category' => 1,
        'class' => 'cat-item cat-item-1'

    ) ); ?>
						
					</ul>
				<div class="sidebar-divider"></div>
				</div>
			</div><!-- close #sidebar -->