<?php 
/*
  Template Name: Contact Us
*/?>
<?php get_header(); ?>
<div id="page-title">		
			<div class="width-container">
				<h1><?php the_title(); ?></h1>
				<div id="bread-crumb">
					<span class="you-are-here-pro">You are here:</span>
					<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Back to home" href="<?php echo home_url(); ?>" class="home"> Home </a></span> &gt; <span typeof="v:Breadcrumb"><span property="v:title"><?php the_title(); ?></span></span></div>
				<div class="clearfix"></div>
			</div>
		</div>
	
	
	</div>
	
	<script type="text/javascript">jQuery(document).ready(function($) {   $("#transparent-header-pro").backstretch([ "images/demo/page-title-1.jpg" ],{ fade: 750, }); }); </script>

	<div id="map-progression">
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3021.436848951956!2d-73.9656126!3d40.774409299999995!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c258a29d3847f5%3A0x564dfbba0141774a!2s5th+Ave%2C+New+York%2C+NY%2C+USA!5e0!3m2!1sen!2sbe!4v1411556637165" frameborder="0"></iframe>
	</div>
	
	<div id="main">
		<div class="width-container bg-sidebar-pro">
			<div id="sidebar-border">				
				<div id="content-container">
					<div class="content-container-pro">
					<?php the_field('form'); ?>
						<h3>Send us a message</h3>
							<form class="progression-contact" id="CommentForm" method="post" action="#">
								<fieldset>
									<div>
										<p><input id="ContactName" name="ContactName" class="textInput required" placeholder="Name" /></p>
									</div>
									<div>
										<p><input id="ContactEmail" name="ContactEmail" class="textInput required email" placeholder="E-mail" /></p>
									</div>
									<div>
										<p><input id="ContactPhone" name="ContactPhone" class="textInput digits" value="" placeholder="Phone" /></p>
									</div>
									<div>
										<p><textarea id="ContactComment" name="ContactComment" class="textInput required" rows="10" cols="4" placeholder=" Your Message"></textarea></p>
									</div>
									<div>
										<p><button type="submit" class="progression-contact-submit"><span>Send Us Your Message</span></button></p>
									</div>
								</fieldset>
							</form>	

					</div><!-- close .content-container-pro -->
				</div>
				
                <div id="sidebar">
				<div class="sidebar-item widget">
					<h6 class="widget-title">Contact us</h6>
					<div class="social-icons-widget-pro">
						<div><strong>Address</strong>: <?php the_field('street_address', 'option'); ?>  <br><?php the_field('provence', 'option'); ?><br><br> <strong>Phone</strong>: <?php the_field('phone_number', 'option'); ?><br> <strong>Mobile</strong>: <?php the_field('mobile_number', 'option'); ?></div>
						<div class="social-ico">
					<?php 
					if( get_field('facebook', 'option') == 'Yes' ): { ?> 
						<a href="<?php the_field('facebook_page_url', 'option'); ?>" target="_blank">
							<i class="fa fa-facebook"></i>
						</a>
						<?php } endif; ?>
						<?php if(get_field('twitter','option') == 'Yes'): { ?>
						<a href="<?php the_field('twitter_link', 'option'); ?>" target="_blank">
							<i class="fa fa-twitter"></i>
						</a>
						<?php } endif ?>
						<?php if(get_field('google_plus','option') == 'Yes'): { ?>
						<a href="<?php the_field('google_plus_link', 'option'); ?>" target="_blank">
							<i class="fa fa-google-plus"></i>
						</a>
						<?php } endif ?>
						<?php if(get_field('email', 'option') == 'Yes'): { ?>
						<a href="mailto:<?php the_field('email_address', 'option'); ?>" target="_blank">
							<i class="fa fa-envelope"></i>
						</a>
						<?php } endif ?>
				</div>
					</div>
					<div class="sidebar-divider"></div>
				</div>
			</div>
		
			<div class="clearfix"></div>
			</div>
		</div>


	<div class="clearfix"></div>		

 <?php get_footer(); ?>