<?php
    /**
     * The functions file is used to initialize everything in the theme.  It controls how the theme is loaded and
     * sets up the supported features, default actions, and default filters.  If making customizations, users
     * should create a child theme and make changes to its functions.php file (not this one).  Friends don't let
     * friends modify parent theme files. ;)
     *
     * Child themes should do their setup on the 'after_setup_theme' hook with a priority of 11 if they want to
     * override parent theme features.  Use a priority of 9 if wanting to run before the parent theme.
     *
     * This program is free software; you can redistribute it and/or modify it under the terms of the GNU
     * General Public License as published by the Free Software Foundation; either version 2 of the License,
     * or (at your option) any later version.
     *
     * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
     * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
     *
     * You should have received a copy of the GNU General Public License along with this program; if not, write
     * to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
     *
     * @package Radouga Distilleries
     * @subpackage Functions
     * @version 1.0
     * @author James Anderson <james.anderson@innovativeimaginations.com>
     * @copyright Copyright (c) 2016, James Anderson
     * @link http://innovativeimaginations.com
     * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
     */

    /* Load the core theme framework. */
    require_once( trailingslashit( get_template_directory() ) . 'library/hybrid.php' );
    new Hybrid();

    /* Do theme setup on the 'after_setup_theme' hook. */
      add_action( 'after_setup_theme', 'radouga_distilleries_setup', 10);

    if (!function_exists('radouga_distilleries_setup')){

        /**
         * Radouga Distilleries Theme setup.
         *
         * Set up theme defaults and registers support for various WordPress features.
         *
         * Note that this function is hooked into the after_setup_theme hook, which
         * runs before the init hook. The init hook is too late for some features, such
         * as indicating support post thumbnails.
         *
         * @since Fat Cat Media House WordPress Theme 1.0
         */

        function radouga_distilleries_setup(){
            /* Add theme support from WP Core. */
            
            // adding support for theme customizer logo
            add_theme_support('custom-logo', array(
                'size' => 'mytheme-logo'
            ));                 
                         
            /*  This theme uses wp_nav_menu() in one location. */
            register_nav_menus( array(
                'front_page_menu' => __( 'Front Page Menu', 'radouga_distilleries'),
                'default_page_menu' => __( 'Default Page Menu', 'radouga_distilleries'),
            ));

            /*  Here are Hybrid Core Framework Extensions which provide more 
             *  Theme Supports
             */
             /*  This is to activate the featured image in Posts
             *  The best thumbnail/image script ever. 
             */
            add_theme_support( 'get-the-image' );

            /* Enable custom template hierarchy. */
            add_theme_support( 'hybrid-core-template-hierarchy' );

            /* Load shortcodes. */
            add_theme_support( 'hybrid-core-shortcodes' );

            /*  Adds input fields in the post editor for adding post-specific 
             *  meta information as well as sets up some defaults on other pages.
             */
            add_theme_support( 'hybrid-core-seo' );
                         
            /* Breadcrumbs. Yay! */
            add_theme_support( 'breadcrumb-trail' );
            
            remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
            remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
            remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
            remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
            remove_action( 'wp_head', 'index_rel_link' ); // index link
            remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
            remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
            remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
            remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP ver
            
            // Hook into the header to add the needed meta 
            add_action('wp_head', 'add_meta_tags', 0);

            // Hook into the 'wp_enqueue_scripts' action
            add_action( 'wp_enqueue_scripts', 'radouga_distilleries_theme_scripts' );
            
            //Set Image Sizes
            //add_action( 'init', 'radouga_distilleries_add_image_sizes' );
            
            /* Filters hooks go here. */

            //enable automatic updates for all plugins
            add_filter( 'auto_update_plugin', '__return_true' );

            // enable automatic updates for all themes
            add_filter( 'auto_update_theme', '__return_true' );
            
            // change the sizes of site iconv_set_encoding
            //add_filter('site_icon_image_sizes', 'add_my_site_icon_sizes');
            
            // change the output of site icon meta tags
            //add_filter('site_icon_meta_tags', 'add_my_site_icon_meta_tags');
            
            /** Gravity Forms Filters 
             *  These functions are located in includes/forms.php  
            **/
            // Add custom classes to inputs
            //add_action("gform_field_input", "gf_custom_class", 10, 5);
 
            /** Gravity Forms Filters 
              * These functions are located in includes/forms.php    
            **/

            // This function forces jQuery calls to be loaded in the footer after all other scripts
            add_filter("gform_init_scripts_footer", "init_scripts");

            // filter the Gravity Forms button type 
            //add_filter("gform_submit_button", "form_submit_button", 10, 2);

            // Add a class to menu icon
            //add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
            
            //Add Prev & Next Link Styled Paginaiton
            add_filter('next_posts_link_attributes', 'next_page_posts_link_attributes');
            add_filter('previous_posts_link_attributes', 'previous_page_posts_link_attributes');

            function theme_prefix_header_custom_logo() {
	
	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}
 
}
 
add_filter('get_custom_logo','change_logo_class');
 
 
function change_logo_class($html)
{
	$html = str_replace('custom-logo', '', $html);
	return $html;
}
            /**
            * Create Advance Custom Fields Options Pages.
            *
            */
            require_once( trailingslashit( THEME_DIR ) .'includes/options-pages.php');

            /**
            * Register our sidebars and widgetized areas.
            *
            */
            require_once( trailingslashit( THEME_DIR ) .'includes/dynamic-sidebars.php');

            /**
            * Create Breadcrumbs
            *
            */
            require_once( trailingslashit( THEME_DIR ) .'includes/breadcrumbs.php');

            /**
            * Gravity Forms Configuration
            *
            */
            require_once( trailingslashit( THEME_DIR ) .'includes/forms.php');

            /**
            * Create Pagination
            *
            */
            require_once( trailingslashit( THEME_DIR ) .'includes/pagination.php');

            /**
            * Customize Nav Menu
            */
            require_once( trailingslashit( THEME_DIR ) .'includes/walker-menu.php');
        }
    }
    
    //adds meta tags along with the title tag to the header.php file
    if (!function_exists('add_meta_tags')) {
        function add_meta_tags() {
        ?>
            
        <?php
        }
    }


    if (!function_exists('the_slug')) {
        // Get the slug
        function the_slug() {
            $post_data = get_post($post->ID, ARRAY_A);
            $slug = $post_data['post_name'];
            return $slug; 
        }
    }
    
    if (!function_exists('special_nav_class')) {
        function special_nav_class($classes, $item){
             if( in_array('menu-item', $classes) ){
                     $classes[] = 'dropdown';
             }
             return $classes;
        }  
    }

    //Set Image Sizes
    if (!function_exists('radouga_distilleries_add_image_sizes')) {
        function radouga_distilleries_add_image_sizes() {
           //add_image_size('featured-image-name', width, height, true/false);
           add_image_size('mytheme-logo', 150, 150);
        }
    }
    
    if(!function_exists('mytheme_custom_logo')){
        function mytheme_custom_logo() {
            // Try to retrieve the Custom Logo
            $output = '';
            if (function_exists('get_custom_logo'))
                $output = get_custom_logo();

            // Nothing in the output: Custom Logo is not supported, or there is no selected logo
            // In both cases we display the site's name
            if (empty($output))
                $output = '<h1><a href="' . esc_url(home_url('/')) . '">' . get_bloginfo('name') . '</a></h1>';

            echo $output;
        }
    }    
    
    if(!function_exists('add_my_site_icon_sizes')){
        function add_my_site_icon_sizes($sizes) {
            $sizes[] = 64;

            return $sizes;
        }
    }
        
    if(!function_exists('add_my_site_icon_meta_tags')){
        function add_my_site_icon_meta_tags($tags) {
            $tags[] = sprintf('', esc_url(get_site_icon_url(null, 64)));

            return $tags;
        }
    }  
    
    // Register Script
    if(!function_exists('radouga_distilleries_theme_scripts')){
        function radouga_distilleries_theme_scripts(){
            global $wp_scripts;
            global $wp_styles;
            

wp_deregister_style( 'bootstrap' );
wp_register_style( 'bootstrap','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', false, '1.0');
wp_enqueue_style( 'bootstrap' );

wp_deregister_style( 'style' );
wp_register_style( 'style',trailingslashit( THEME_URI ) .'style.css', false, '1.0');
wp_enqueue_style( 'style' );

wp_deregister_style( 'custom' );
wp_register_style( 'custom',trailingslashit( THEME_URI ) .'custom.css', false, '1.0');
wp_enqueue_style( 'custom' );

wp_deregister_style( 'responsive' );
wp_register_style( 'responsive',trailingslashit( THEME_URI ) .'css/responsive.css', false, '1.0');
wp_enqueue_style( 'responsive' );

wp_deregister_style( 'merriweather' );
wp_register_style( 'merriweather', 'http://fonts.googleapis.com/css?family=Raleway:300,400,600,700|Merriweather:400,400italic,700', false, '1.0');
wp_enqueue_style( 'merriweather' );

wp_deregister_style( 'settings' );
wp_register_style( 'settings',trailingslashit( THEME_URI ) .'rs-plugin/css/settings.css', false, '1.0');
wp_enqueue_style( 'settings' );

wp_deregister_style( 'navigation' );
wp_register_style( 'navigation',trailingslashit( THEME_URI ) .'rs-plugin/css/navigation.css', false, '1.0');
wp_enqueue_style( 'navigation' );

wp_deregister_script( 'scripts' );
wp_register_script( 'scripts',trailingslashit( THEME_URI ) .'js/build/scripts.js', false, '1.0');
wp_enqueue_script( 'scripts' );

// wp_deregister_script( 'jquery' );
// wp_register_script( 'jquery',trailingslashit( THEME_URI ) .'js/libs/jquery-1.7.1.min.js', false, '1.0');
// wp_enqueue_script( 'jquery' );

// wp_deregister_script( 'modernizr' );
// wp_register_script( 'modernizr',trailingslashit( THEME_URI ) .'js/libs/modernizr-2.6.2.min.js', false, '1.0');
// wp_enqueue_script( 'modernizr' );

// wp_deregister_script( 'plugins' );
// wp_register_script( 'plugins',trailingslashit( THEME_URI ) .'js/plugins.js', false, '1.0');
// wp_enqueue_script( 'plugins' );

// wp_deregister_script( 'script' );
// wp_register_script( 'script',trailingslashit( THEME_URI ) .'js/script.js', false, '1.0');
// wp_enqueue_script( 'script' );

// wp_deregister_script( 'themepunchtools' );
// wp_register_script( 'themepunchtools',trailingslashit( THEME_URI ) .'rs-plugin/js/jquery.themepunch.tools.min.js', false, '1.0');
// wp_enqueue_script( 'themepunchtools' );

// wp_deregister_script( 'themepunchrev' );
// wp_register_script( 'themepunchrev',trailingslashit( THEME_URI ) .'rs-plugin/js/jquery.themepunch.revolution.min.js', false, '1.0');
// wp_enqueue_script( 'themepunchrev' );

// wp_deregister_script( 'revactions' );
// wp_register_script( 'revactions',trailingslashit( THEME_URI ) .'rs-plugin/js/extensions/revolution.extension.actions.min.js', false, '1.0', true);
// wp_enqueue_script( 'revactions' );

// wp_deregister_script( 'revcarousel' );
// wp_register_script( 'revcarousel',trailingslashit( THEME_URI ) .'rs-plugin/js/extensions/revolution.extension.carousel.min.js', false, '1.0', true);
// wp_enqueue_script( 'revcarousel' );

// wp_deregister_script( 'revkenburn' );
// wp_register_script( 'revkenburn',trailingslashit( THEME_URI ) .'rs-plugin/js/extensions/revolution.extension.kenburn.min.js', false, '1.0', true);
// wp_enqueue_script( 'revkenburn' );

// wp_deregister_script( 'revlayer' );
// wp_register_script( 'revlayer',trailingslashit( THEME_URI ) .'rs-plugin/js/extensions/revolution.extension.layeranimation.min.js', false, '1.0', true);
// wp_enqueue_script( 'revlayer' );

// wp_deregister_script( 'revmigration' );
// wp_register_script( 'revmigration',trailingslashit( THEME_URI ) .'rs-plugin/js/extensions/revolution.extension.migration.min.js', false, '1.0', true);
// wp_enqueue_script( 'revmigration' );

// wp_deregister_script( 'revnavigation' );
// wp_register_script( 'revnavigation',trailingslashit( THEME_URI ) .'rs-plugin/js/extensions/revolution.extension.navigation.min.js', false, '1.0', true);
// wp_enqueue_script( 'revnavigation' );

// wp_deregister_script( 'revparallax' );
// wp_register_script( 'revparallax',trailingslashit( THEME_URI ) .'rs-plugin/js/extensions/revolution.extension.parallax.min.js', false, '1.0', true);
// wp_enqueue_script( 'revparallax' );

// wp_deregister_script( 'revslideanims' );
// wp_register_script( 'revslideanims',trailingslashit( THEME_URI ) .'rs-plugin/js/extensions/revolution.extension.slideanims.min.js', false, '1.0', true);
// wp_enqueue_script( 'revslideanims' );

// wp_deregister_script( 'revvideo' );
// wp_register_script( 'revvideo',trailingslashit( THEME_URI ) .'rs-plugin/js/extensions/revolution.extension.video.min.js', false, '1.0', true);
// wp_enqueue_script( 'revvideo' );


        }
    }
