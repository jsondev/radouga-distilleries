<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="UTF-8"/>
<meta name="viewport" content="initial-scale=1,user-scalable=no,maximum-scale=1">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>
</head>
<body <?php body_class();?>>
<div id="wrapper">
      <div id="header">
      	<a href="<?php echo home_url(); ?>">
			<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php bloginfo( 'name' ); ?>" />
            </a>                               
            <div class="header-bot">
                  <?php wp_nav_menu(  array('theme_location' => 'social' , 'menu_class' => 'social',  'container' => false)); ?>
            </div>            
            	<div class="sidebar hide-large">
					<a href="#" id="pull">
						<?php _e('Menu','mountaincreek');?>
					</a>
            <?php wp_nav_menu(  
	            	array('theme_location' => 'sidebar-nav' ,
	            	  'menu_class' => 'sidebar-nav rounds',
	            	  'link_before' => '<span class="screen-reader-text">',
	            	  'link_after' => '</span>',
	            	  'container' => false
	            	  )
	            	);
	            ?>
            </div>
      </div>