msgid ""
msgstr ""
"Project-Id-Version: Nuovo\n"
"POT-Creation-Date: 2015-07-27 17:32-0500\n"
"PO-Revision-Date: 2015-07-27 17:38-0500\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: es_MX\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.3\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-WPHeader: style.css\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: 404.php:21
msgid ""
"We're sorry, but the post or page you are looking for is not here. It may "
"have been removed before you got here or you have a bad link. You can either "
"go back to the "
msgstr ""
"Lo sentimos, pero el poste o la página que busca no está aquí. Se puede se "
"han eliminado antes de que llegaras o tienes un enlace. Tampoco puedes "
"volver a la"

#: 404.php:21
msgid "homepage"
msgstr "página principal"

#: 404.php:21
msgid " or use the search form below to find another post."
msgstr " o uso la búsqueda forma debajo para encontrar otro post."

#: admin/theme-options.php:18
msgid "General Settings"
msgstr "Configuración General"

#: admin/theme-options.php:19
msgid "This is the general settings section."
msgstr "Esta es la sección de ajustes generales."

#: admin/theme-options.php:36
msgid "Custom RSS Feed"
msgstr "Canal RSS personalizado"

#: admin/theme-options.php:54
msgid "Color Theme"
msgstr "Tema de Color"

#: admin/theme-options.php:58 admin/theme-options.php:504
msgid "Default"
msgstr "Defecto"

#: admin/theme-options.php:59 admin/theme-options.php:505
msgid "Blue"
msgstr "Azul"

#: admin/theme-options.php:60 admin/theme-options.php:506
msgid "Green"
msgstr "Verde"

#: admin/theme-options.php:61 admin/theme-options.php:507
msgid "Orange"
msgstr "Naranja"

#: admin/theme-options.php:62 admin/theme-options.php:508
msgid "Purple"
msgstr "Morado"

#: admin/theme-options.php:63 admin/theme-options.php:509
msgid "Red"
msgstr "Rojo"

#: admin/theme-options.php:64 admin/theme-options.php:510
msgid "Yellow"
msgstr "Amarillo"

#: admin/theme-options.php:81 functions.php:28
msgid "Top Menu"
msgstr "Menú Superior"

#: admin/theme-options.php:99
msgid "Author Bio"
msgstr "Bio Autor"

#: admin/theme-options.php:114
msgid "Social Media Settings"
msgstr "Configuración de las redes sociales"

#: admin/theme-options.php:115
msgid "This is the social media settings section."
msgstr "Esta es la sección de configuración de medios de comunicación social."

#: admin/theme-options.php:132
msgid "Facebook Link"
msgstr "Enlace a Facebook"

#: admin/theme-options.php:150
msgid "Twitter Link"
msgstr "Enlace a Twitter"

#: admin/theme-options.php:168
msgid "YouTube Link"
msgstr "Enlace a YouTube"

#: admin/theme-options.php:186
msgid "Google+ Link"
msgstr "Enlace de Google+"

#: admin/theme-options.php:204
msgid "LinkedIn Link"
msgstr "Enlace a Linkedin"

#: admin/theme-options.php:222
msgid "Instagram Link"
msgstr "Enlace a Instagram"

#: admin/theme-options.php:240
msgid "Tumblr Link"
msgstr "Enlace a Tumblr"

#: admin/theme-options.php:258
msgid "Pinterest Link"
msgstr "Enlace a Pinterest"

#: admin/theme-options.php:272 admin/theme-options.php:532
msgid "None"
msgstr "Ninguno"

#: admin/theme-options.php:280
msgid "Homepage Options"
msgstr "Opciones de página"

#: admin/theme-options.php:281
msgid "This is the homepage settings section."
msgstr "Esta es la sección de configuración de página principal."

#: admin/theme-options.php:298
msgid "Slideshow Category"
msgstr "Categoría Slideshow"

#: admin/theme-options.php:317
msgid "Number of Slideshow Posts"
msgstr "Número de entradas para la presentación de diapositivas"

#: admin/theme-options.php:335
msgid "First Category"
msgstr "Primera Categoría"

#: admin/theme-options.php:354
msgid "Number of Posts in the First Section"
msgstr "Número de puestos en la primera sección"

#: admin/theme-options.php:372
msgid "Second Category"
msgstr "Segunda Categoría"

#: admin/theme-options.php:391
msgid "Number of Posts in the Second Section"
msgstr "Número de historias en la segunda columna"

#: admin/theme-options.php:409
msgid "Third Category"
msgstr "Tercera Categoría"

#: admin/theme-options.php:428
msgid "Number of Posts in the Third Section"
msgstr "Número de historias en la tercera columna"

#: admin/theme-options.php:446
msgid "Fourth Category"
msgstr "Cuarta Categoría"

#: admin/theme-options.php:465
msgid "Number of Posts in the Fourth Section"
msgstr "Número de entradas para la presentación de diapositivas"

#: admin/theme-options.php:483
msgid "Number of Latest Posts"
msgstr "Número de últimos mensajes"

#: archive.php:23 author.php:24 home.php:69 home.php:104 home.php:140
#: home.php:176 index.php:24
msgid "Off"
msgstr "Apagado"

#: archive.php:29 author.php:30 functions.php:313 home.php:34 home.php:73
#: home.php:108 home.php:144 home.php:180 home.php:209 index.php:30
msgid "Written By: "
msgstr "Escrito por: "

#: archive.php:29 author.php:30 index.php:30
msgid "on"
msgstr "en"

#: archive.php:31 author.php:32 home.php:35 index.php:32
msgid "Posted in: "
msgstr "Publicado en: "

#: archive.php:40 author.php:40 index.php:40
msgid "<div class=\"next-posts\">Older Posts&rsaquo;&rsaquo;</div>"
msgstr "<div class=“next-posts”>Entradas antiguos&rsaquo;&rsaquo;</div>"

#: archive.php:41 author.php:41 index.php:41
msgid "<div class=\"previous-posts\">&lsaquo;&lsaquo;Newer Posts</div>"
msgstr ""
"<div class=“previous-posts”>&lsaquo;&lsaquo;Entradas más recientes</div>"

#: author.php:16
msgid "Posts by "
msgstr "Publicaciones por: "

#: comments.php:17
msgid "This post is password protected. Enter the password to view comments."
msgstr ""
"Esta entrada está protegida. Introduzca la contraseña para ver los "
"comentarios."

#: comments.php:23
msgid "No Responses"
msgstr "No hay respuestas"

#: comments.php:23
msgid "One Response"
msgstr "Una respuesta"

#: comments.php:23
msgid "% Responses"
msgstr "% Respuestas"

#: comments.php:23
msgid "to"
msgstr "a"

#: comments.php:38
msgid "Comments are closed."
msgstr "Los comentarios están cerrados."

#: footer.php:15
msgid "Copyright"
msgstr "Derechos de autor"

#: functions.php:29
msgid "Main Menu"
msgstr "Menú Principal"

#: functions.php:82
msgid "Sidebar"
msgstr "Barra lateral"

#: functions.php:101
msgid "Continue Reading&rsaquo;&rsaquo;"
msgstr "Seguir leyendo &rsaquo; &rsaquo;"

#: functions.php:292 home.php:35 home.php:209
msgid "0 Comments"
msgstr "0 Comentarios"

#: functions.php:294 home.php:35 home.php:209
msgid "1 Comment"
msgstr "1 Comentario"

#: functions.php:296
msgid " Comments"
msgstr " Comentarios"

#: functions.php:299 home.php:35
msgid "Comments Off"
msgstr "Comentarios desactivados"

#: functions.php:316
msgid "Tags: "
msgstr "Etiquetas: "

#: functions.php:331 functions.php:334 functions.php:337
msgid "Archives for "
msgstr "Archivos para "

#: functions.php:343
msgid "Search results for "
msgstr "Mostrando resultados para: "

#: functions.php:350 index.php:16
msgid "Page "
msgstr "Página"

#: functions.php:367
#, php-format
msgid "%s"
msgstr "%s"

#: functions.php:368
#, php-format
msgid "%1$s at %2$s"
msgstr "%1$s at %2$s"

#: functions.php:368
msgid "(Edit)"
msgstr "(Editar)"

#: functions.php:373
msgid "Your comment is awaiting moderation."
msgstr "Su comentario está esperando a que sea revisado."

#: functions.php:390
msgid "del"
msgstr "borrar"

#: functions.php:391
msgid "spam"
msgstr "correo no deseado"

#: functions.php:407
msgid "About "
msgstr "Acerca de "

#: header.php:52
msgid "Go to..."
msgstr "Ir a…"

#: home.php:35 home.php:209
msgid "% Comments"
msgstr "% Comentarios"

#: home.php:80 home.php:115 home.php:151 home.php:187
msgid "View All &rsaquo;&rsaquo;"
msgstr "Ver todos los &rsaquo; &rsaquo;"

#: home.php:194
msgid "Latest Posts"
msgstr "Últimas entradas"

#: home.php:209
msgid "Comments Closed"
msgstr "Comentarios Cerrados"

#: home.php:214
msgid "<span class=\"next-posts\">Next Posts&rsaquo;&rsaquo;</span>"
msgstr "<span class=“next-posts”>Siguiente Mensajes&rsaquo;&rsaquo;</span>"

#. Theme Name of the plugin/theme
msgid "Nuovo"
msgstr ""

#. Theme URI of the plugin/theme
msgid "http://www.jacobmartella.com/nuovo-wordpress-theme/"
msgstr ""

#. Description of the plugin/theme
msgid ""
"Nuovo gives the user almost complete control over the look and feel of his "
"or her website. Users can select the color theme they want, what posts they "
"want to show up in the homepage slider as well as in four separate areas on "
"the homepage and can display the social media links they want in the header "
"— all from the Customizer. Nuovo is also responsive, making it look good on "
"any device with no extra work required."
msgstr ""

#. Author of the plugin/theme
msgid "Jacob Martella"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://www.jacobmartella.com/"
msgstr ""
