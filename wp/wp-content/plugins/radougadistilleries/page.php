<?php get_header(); ?>
<?php if(have_posts()) : ?>
	    <?php while (have_posts()) : the_post(); ?>
		<?php the_content(); ?>>

<?php endwhile; ?>
	      
	    <?php else : ?>
	    <?php wp_reset_query(); ?>
	      <h1>No Post Found</h1>
	  	<?php get_search_form( ); ?>

	<?php endif; ?>

<?php get_footer(); ?>