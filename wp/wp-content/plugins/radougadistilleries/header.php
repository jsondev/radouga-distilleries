	<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="UTF-8">
	<title><?php wp_title(); ?></title>
			<meta name="keywords" content="provincial vodka" />
		<meta name="description" content="">
		<meta name="author" content="James Anderson">
	<meta name="viewport" content="width=device-width">
    <!-- Favicon -->
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
	
<?php wp_head(); ?>
</head>
<body>
<header>
	<div id="header-top">
		<div class="width-container">
			<div id="additional-social-pro">
				<div class="social-ico">
					<?php 
					if( get_field('facebook', 'option') == 'Yes' ): { ?> 
						<a href="<?php the_field('facebook_page_url', 'option'); ?>" target="_blank">
							<i class="fa fa-facebook"></i>
						</a>
						<?php } endif; ?>
						<?php if(get_field('twitter','option') == 'Yes'): { ?>
						<a href="<?php the_field('twitter_link', 'option'); ?>" target="_blank">
							<i class="fa fa-twitter"></i>
						</a>
						<?php } endif ?>
						<?php if(get_field('google_plus','option') == 'Yes'): { ?>
						<a href="<?php the_field('google_plus_link', 'option'); ?>" target="_blank">
							<i class="fa fa-google-plus"></i>
						</a>
						<?php } endif ?>
						<?php if(get_field('email', 'option') == 'Yes'): { ?>
						<a href="mailto:<?php the_field('email_address', 'option'); ?>" target="_blank">
							<i class="fa fa-envelope"></i>
						</a>
						<?php } endif ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
		<div id="pro-header-white">
			<div class="width-container">
				<h1 id="logo">
					
						<?php theme_prefix_header_custom_logo()?>
					
				</h1>
				<nav>
					<div class="menu-main-navigation-container">
					<?php 
                    wp_nav_menu( array(
                        'theme_location'    => 'front_page_menu',
                        'container'     => '',
                        'container_class' => '',
                        'menu_class'        => 'sf-menu', 
						'menu_id' => 'menu-main-navigation',
                        'items_wrap'        => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'walker'        => new custom_theme_walker_nav_menu
                        ) ); 
                        ?>
					</div>
				</nav>
				<div class="clearfix"></div>
			</div>
		</div>
	</header>