<?php get_header(); ?>
<div id='slider-container' class='container-fluid'>
	<div class='row'>
	<div class='col-lg-5 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 slider-margin'>
		
	
<div id="pro-home-slider"><!-- START REVOLUTION SLIDER -->
	<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
		<div id="rev_slider_1_1" class="rev_slider fullwidthabanner">
		<ul>
		<?php
		// check if the repeater field has rows of data
		if( have_rows('home_page_slider_repeater') ):
			 // loop through the rows of data
			 $i = 1;
				while ( have_rows('home_page_slider_repeater') ) : the_row();
				
				?>
					
<!-- SLIDE  -->
			<li data-transition="zoomin" data-slotamount="7" data-masterspeed="300" data-thumb="<?php the_sub_field('slider_image'); ?>"  data-saveperformance="off"  data-title="Slide <?php echo $i; ?>">
				<!-- MAIN IMAGE -->
				<img src="rs-plugin/images/dummy.png"  alt="sample-image47" data-lazyload="<?php the_sub_field('slider_image'); ?>" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
			
			<!-- LAYERS -->

			<!-- LAYER NR. 1 -->
			<div class="tp-caption triven-heading tp-fade tp-resizeme"
				data-x="130"
				data-y="bottom" data-voffset="252"
				data-speed="300"
				data-start="750"
				data-easing="Power1.easeIn"
				data-splitin="none"
				data-splitout="none"
				data-elementdelay="0.1"
				data-endelementdelay="0.1"
				data-endspeed="300"
				>
			<?php the_sub_field('image_tagline'); ?>
			</div>

			<!-- LAYER NR. 2 -->
			<div class="tp-caption triven-text tp-fade tp-resizeme"
				data-x="130"
				data-y="bottom" data-voffset="177"
				data-speed="300"
				data-start="1200"
				data-easing="Power1.easeIn"
				data-splitin="none"
				data-splitout="none"
				data-elementdelay="0.1"
				data-endelementdelay="0.1"
				data-endspeed="300"
				><?php the_sub_field('tagline_explanation'); ?>
			</div>

			<!-- LAYER NR. 3 -->
			<div class="tp-caption small_light_white tp-fade tp-resizeme"
				data-x="130"
				data-y="bottom" data-voffset="70"
				data-speed="300"
				data-start="1200"
				data-easing="Power1.easeIn"
				data-splitin="none"
				data-splitout="none"
				data-elementdelay="0.1"
				data-endelementdelay="0.1"
				data-endspeed="300"
				>
				<a href="<?php the_sub_field('button_link'); ?>" class="progression-button" target="_self"><span class="progression-button-inner"><?php the_sub_field('button_text'); ?></span></a>
			</div>
		</li>



			<?php
			$i++;
			endwhile;
		else :
		// no rows found
		endif;
		?>	
	</ul>
	<div class="tp-bannertimer tp-bottom"></div>
	</div>
	</div>
				<script type="text/javascript">


				/******************************************
					-	PREPARE PLACEHOLDER FOR SLIDER	-
				******************************************/			
				
                var tpj = jQuery;

                var revapi8;
                tpj(document).ready(function() {
                    if (tpj("#rev_slider_1_1").revolution == undefined) {
                        revslider_showDoubleJqueryError("#rev_slider_1_1");
                    } else {
                        revapi8 = tpj("#rev_slider_1_1").show().revolution({
                            sliderType: "standard",
                            jsFileLocation:"rs-plugin/js/",
                            sliderLayout: "auto",
                            delay: 9000,
                            navigation: {
                                keyboardNavigation: "on",
                                keyboard_direction: "horizontal",
                                mouseScrollNavigation: "off",
                                onHoverStop: "off",
                                touch: {
                                    touchenabled: "on",
                                    swipe_threshold: 75,
                                    swipe_min_touches: 1,
                                    swipe_direction: "horizontal",
                                    drag_block_vertical: false
                                },
                                arrows: {
                                    style: "uranus",
                                    enable: true,
                                    hide_onmobile: true,
                                    hide_onleave: false,
                                    tmp: '',
                                    left: {
                                        h_align: "left",
                                        v_align: "center",
                                        h_offset: 10,
                                        v_offset: 0
                                    },
                                    right: {
                                        h_align: "right",
                                        v_align: "center",
                                        h_offset: 10,
                                        v_offset: 0
                                    }
                                },
                                bullets: {
                                    enable: true,
                                    hide_onmobile: true,
                                    style: "uranus",
                                    hide_onleave: false,
                                    direction: "horizontal",
                                    h_align: "center",
                                    v_align: "bottom",
                                    h_offset: 20,
                                    v_offset: 30,
                                    space: 5,
                                    tmp: '<span class="tp-bullet-inner"></span>'
                                }
                            },
gridwidth:1230,
      gridheight:720,
							disableProgressBar: "on",
							spinner: "off",							
                            lazyType: "single",
                            debugMode: false,
                            fallbacks: {
                                simplifyAll: "off",
                                nextSlideOnWindowFocus: "off",
                                disableFocusListener: false,
                            }
                        });
                    }
                }); /*ready*/				

				</script>

	<!-- END REVOLUTION SLIDER -->
	</div>
	</div>
		<div class='col-lg-4 col-md-12 col-sm-12 col-xs-12'>
			
			<?php the_field('intro_content');?>
		</div>
</div>
	</div>

	 <div id="address-hours-homepage">
		<div class="width-container">
			<div id="address-homepage-pro"><?php the_field('company_address'); ?></div>
			<div id="hours-homepage-pro"><?php the_field('operation_hours'); ?></div> 
		</div>
	</div>	
		
		
	<div id="main">
		<div class="width-container">
		
				<h2><?php the_field('about_us_title'); ?></h2>
				<h4><?php the_field('about_us_subtitle'); ?></h4>
					<div class="ls-sc-grid_6 alpha">
						<?php the_field('about_us_content'); ?>
					</div>
					<div class="ls-sc-grid_6 omega">
						<img src="<?php the_field('about_us_image'); ?>" />
					</div>
					<div class="clear"></div>
					
		<hr class="ls-sc-divider dotted grey "/>
		<h2><?php the_field('staff_title'); ?></h2>
		<h4><?php the_field('staff_subtitle'); ?></h4>
		<div class='clearfix'></div>
		<!-- Homepage Child Pages Start -->
			<?php if( have_rows('staff_repeater') ):
					   while ( have_rows('staff_repeater') ) : the_row(); ?>
<div class="home-child-boxes grid3column-progression ">
				<div class="home-child-boxes-container">
					<div class="home-child-image-pro">
						<?php the_sub_field('staff_profile_picture'); ?>
					</div>				
					<h5 class="home-child-title"><?php the_sub_field('staff_name'); ?></h5>
					<p><?php the_sub_field('staff_member_description'); ?></p>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
					   <?php endwhile; ?>
			<?php else : ?>
			<?php endif; ?>
		<!-- Homepage Child Pages End -->
		
		<div class="clearfix"></div>
	</div><!-- close .width-container -->

	<div class="clearfix"></div>
	</div><!-- close #main -->

	<div class="widget home-widget pyre_homepage_media-menu">		
					
		<div class="pyre_homepage_media-widget-menu-3 portfolio-posts-home">
			<div class="width-container">
					<h2 class="home-widget"><?php the_field('latest_posts'); ?></h2>
					<div class="summary-portfolio-pro">
						<h4><?php the_field('latest_post_tagline'); ?></h4>
					</div>			
<?php $query = new WP_Query(array('post_type' => 'post','orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => '3') ); ?>
	<?php if($query->have_posts()) : ?>
	        <?php while ($query->have_posts()) : $query->the_post(); ?>
					<div class=" home-portfolio-boxes grid3column-progression">
						<article>
							<div class="menu-index-pro">
								<div class="menu-image-pro-home">
									<a href="<?php the_field('blog_image'); ?>" rel="prettyPhoto">
										<img width="800" height="400" src="<?php the_field('blog_image'); ?>" alt="mozzarella-1">
									</a>
								</div>
								<div class="menu-content-pro-home">
									<h5><?php the_title(); ?></h5>
									<div class="menu_content_pro-home">
									<?php $summary = get_field('blog_entry'); 
          echo substr($summary, 0, 260) . "..."; ?>
								</div>
									<a href="<?php the_permalink(); ?>">Read More</a>
								</div>
							<div class="clearfix"></div>
							</div><!-- close .menu-index-pro -->
						</article>

						<div class="clearfix"></div>
					</div><!-- close .grid -->
											        <?php endwhile; ?>
	<?php else : ?>
	  <?php wp_reset_query(); ?>

	<?php endif; ?>
			
			<div class="clearfix"></div>							
				
			</div><!-- close .width-container -->
		</div><!-- close .portfolio-posts-home -->
	</div>
	
	

<?php get_footer(); ?>