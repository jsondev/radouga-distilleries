<?php 
/*
  Template Name: Template D
*/?>
<?php get_header(); ?>
<div id="page-title">		
			<div class="width-container">
				<h1><?php the_title(); ?></h1>
				<div id="bread-crumb">
					<span class="you-are-here-pro">You are here:</span>
					<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Back to home" href="<?php echo home_url(); ?>" class="home"> Home </a></span> &gt; <span typeof="v:Breadcrumb"><span property="v:title"><?php the_title(); ?></span></span></div>
				<div class="clearfix"></div>
			</div>
		</div>
        	<div id="main">
		<div class="width-container">
			<div id="full-width-progression">						
				<div id="content-container-full-width">
					<div class="content-container-pro">
                    <div class="ls-sc-grid_4 omega"><?php the_field('column_4'); ?></div>
<div class="ls-sc-grid_8 alpha"><?php the_field('column_8'); ?></div>
							

                    					</div><!-- close .content-container-pro -->
				</div>
				<div class="clearfix"></div>
			</div>
		</div><!-- close .width-container -->
        </div>
 <?php get_footer(); ?>