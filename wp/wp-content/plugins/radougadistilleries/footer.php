<?php wp_footer(); ?>
<footer>
<div id="widget-area">
		<div class="width-container footer-3-column">
			<div class="widget">
				<h2 class="widget-title"><?php the_field('location_title'); ?></h2>
				<div class="social-icons-widget-pro">				
					<div>

						<p><?php the_field('street_address', 'option'); ?> <br> <?php the_field('provence', 'option'); ?></p>
						<p>Phone: <?php the_field('phone_number', 'option'); ?> <br>	Mobile: <?php the_field('mobile_number', 'option'); ?> </p>
					</div>							
					<div class="social-ico">
					<?php 
					if( get_field('facebook', 'option') == 'Yes' ): { ?> 
						<a href="<?php the_field('facebook_page_url', 'option'); ?>" target="_blank">
							<i class="fa fa-facebook"></i>
						</a>
						<?php } endif; ?>
						<?php if(get_field('twitter','option') == 'Yes'): { ?>
						<a href="<?php the_field('twitter_link', 'option'); ?>" target="_blank">
							<i class="fa fa-twitter"></i>
						</a>
						<?php } endif ?>
						<?php if(get_field('google_plus','option') == 'Yes'): { ?>
						<a href="<?php the_field('google_plus_link', 'option'); ?>" target="_blank">
							<i class="fa fa-google-plus"></i>
						</a>
						<?php } endif ?>
						<?php if(get_field('email', 'option') == 'Yes'): { ?>
						<a href="mailto:<?php the_field('email_address', 'option'); ?>" target="_blank">
							<i class="fa fa-envelope"></i>
						</a>
						<?php } endif ?>
					</div><!-- close .social-ico -->
				</div><!-- close .social-icons-widget-pro -->
			</div>
			<!--
			<div class="widget hours"><h2 class="widget-title"><?php the_field('operation_hours' , 'option'); ?></h2>			
				<ul class="open-hours">
					<li>
						<div class="date-day grid2column-progression">Monday</div>
						<div class="hours-date grid2column-progression lastcolumn-progression"><?php the_field('monday', 'option'); ?></div>
						<div class="clearfix"></div>
					</li>
					<li>
						<div class="date-day grid2column-progression">Tuesday</div>
						<div class="hours-date grid2column-progression lastcolumn-progression"><?php the_field('tuesday', 'option'); ?></div>
						<div class="clearfix"></div>
					</li>
					<li>
						<div class="date-day grid2column-progression">Wednesday</div>
						<div class="hours-date grid2column-progression lastcolumn-progression"><?php the_field('wednesday', 'option'); ?></div>
						<div class="clearfix"></div>
					</li>
					<li>
						<div class="date-day grid2column-progression">Thursday</div>
						<div class="hours-date grid2column-progression lastcolumn-progression"><?php the_field('thursday', 'option'); ?></div>
						<div class="clearfix"></div>
					</li>
					<li>
						<div class="date-day grid2column-progression">Friday</div>
						<div class="hours-date grid2column-progression lastcolumn-progression"><?php the_field('friday', 'option'); ?></div>
						<div class="clearfix"></div>
					</li>
					<li>
						<div class="date-day grid2column-progression">Saturday</div>
						<div class="hours-date grid2column-progression lastcolumn-progression"><?php the_field('saturday', 'option'); ?></div>
						<div class="clearfix"></div>
					</li>
					<li>
						<div class="date-day grid2column-progression">Sunday</div>
						<div class="hours-date grid2column-progression lastcolumn-progression"><?php the_field('sunday', 'option'); ?></div>
						<div class="clearfix"></div>
					</li>
				</ul>
			</div>
			-->
			<div class="widget">
				<h2 class="widget-title">Newsletter</h2>
				<div class="textwidget">
					<p>Join our digital mailing list and get news, deals, and be first to know about events at Triven!</p>
					<div id="mc_embed_signup"><form action="//progressionstudios.us1.list-manage.com/subscribe/post?u=1a06aa3bca8613232881e8a6e&amp;id=2f5a556941" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate=""><div class="mc-field-group"> <label for="mce-EMAIL">Email Address </label> <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Your E-mail Address"></div><div id="mce-responses" class="clear"><div class="response" id="mce-error-response" style="display:none"></div><div class="response" id="mce-success-response" style="display:none"></div></div><div style="position: absolute; left: -5000px;"><input type="text" name="b_1a06aa3bca8613232881e8a6e_2f5a556941" tabindex="-1" value=""></div><div class="clear"><input type="submit" value="Go" name="subscribe" id="mc-embedded-subscribe" class="button"></div></form></div>
				</div>
			</div>
		
		<div class="clearfix"></div>
		</div>
	</div>	
		<div id="copyright">
			<div class="width-container">
				<div id="copyrigh-text">© 2014 All Rights Reserved. Developed by ProgressionStudios</div>
				<a class="scrollup" href="#top"><i class="fa fa-angle-up"></i></a>
				<div class="menu-footer-menu-container">
					<ul id="menu-footer-menu" class="pro-footer-menu">
						<li class="current-menu-item">
							<a href="index.html">Home</a>
						</li>
						<li>
							<a href="page-reservation.html">Reservations</a>
						</li>
						<li>
							<a href="blog.html">Latest News</a>
						</li>
						<li>
							<a href="contact.html">Contact</a>
						</li>
					</ul>
				</div>					
			</div><!-- close .width-container -->
			<div class="clearfix"></div>
		</div><!-- close #copyright -->
	</footer>
</body>

</html>