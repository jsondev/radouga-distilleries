<?php get_header('page'); ?>
  <?php if(have_posts()) : ?>
    <?php while (have_posts()) : the_post(); 
	$category = get_the_category();
								$category_title = $category[0]->name; 
								
								?>
      <div id="page-title">
        <div class="width-container">
          <h1><?php wp_title(); ?></h1>
          <div id="bread-crumb">
            <span class="you-are-here-pro">You are here:</span>
            <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Back to home" href="<?php echo home_url(); ?>" class="home"> Home </a></span> &gt; <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to <?php wp_title(); ?>." href="blog.html" class="post-root post post-post"><?php wp_title(); ?></a></span>            &gt; <span typeof="v:Breadcrumb"><span property="v:title"><?php the_title(); ?></span></span>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
		  </div>
	  	<script type="text/javascript">jQuery(document).ready(function($) {   $("#page-title").backstretch([ "http://radougadistilleries.com/wp/wp-content/themes/radougadistilleries/images/0_banner-full_l-1024x453.jpg" ],{ fade: 750,width:300, }); }); </script>
      <div id="main">
        <div class="width-container">
          <div id="single-meta-pro">
            <div id="single-page-title-pro">
              <div class="pro-cat">
                <time class="entry-date" datetime="<?php the_time('F j, Y'); ?>">
                  <?php the_time('F j, Y'); ?>
                </time>
              </div>
              <h2 class="blog-title"><?php the_title(); ?></h2>
            </div>
            <div class="meta-progression"><span class="author-meta-pro">By <?php the_author(); ?></span> <span class="category-meta-pro"><?php echo $category_title; ?></span> <span class="comment-meta-pro"><a href="blog-post.html#respond" title="Comment on <?php the_title(); ?>"><?php
			comments_number( '0', '1', '%' ); ?> Comments</a></span>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- close .width-container -->
        <div class="container">
          <div class="col-md-8 col-md-offset-2">
            <div class="featured-blog-progression">
              <a href="<?php the_field('blog_image'); ?>" rel="prettyPhoto">
				<img src="<?php the_field('blog_image'); ?>" class="img-responsive attachment-progression-blog-single" alt="Restaurant">					
			</a>
            </div>
          </div>
        </div>
        <div class="width-container">
          <div id="content-container">
            <div class="content-container-pro">
              <article>
                <div class="blog-container-text">
                  <div class="entry-content">
                    <?php the_field('blog_entry'); ?>
                  </div>
                  <!-- .entry-content -->
                  <div id="nav-below" class="post-navigation">
                    <h1 class="screen-reader-text">Post navigation</h1>
                    <div class="nav-previous">
					<?php previous_posts_link( '← Previous Article' ); ?>
					</div>
					
                    <div class="nav-next"><a href="<?php next_post_link(); ?>" rel="next">Next Article <span>→</span></a></div>
                  </div>
                  <!-- #nav-below -->

                  <div id="comments" class="comments-area">
                    <h3 class="comments-title"><?php
			comments_number( '0', '1', '%' ); ?> comments on “<span><?php wp_title(); ?></span>”</h3>
                    <ol class="commentlist">
                      <?php comments_template('comments.php'); ?>
                    </ol>
                    <!-- .comment-list -->
                    <div class="clearfix"></div>
                  </div>
                  <!-- #comments -->
                </div>
                <!-- close .blog-container-text -->
                <div class="clearfix"></div>
              </article>
              <!-- #post-## -->
            </div>
            <!-- close .content-container-pro -->
          </div>
          <!-- close #content-container -->
          <?php get_sidebar(); ?>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
      </div>
      <?php endwhile; ?>
        <?php else : ?>
          <?php wp_reset_query(); ?>
            <h1>No Post Found</h1>
            <?php get_search_form( ); ?>
              <?php endif; ?>
                <?php get_footer(); ?>
