<?php 
/*
  Template Name: Template B
*/?>
<?php get_header(); ?>
<div id="page-title">		
			<div class="width-container">
				<h1><?php the_title(); ?></h1>
				<div id="bread-crumb">
					<span class="you-are-here-pro">You are here:</span>
					<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Back to home" href="<?php echo home_url(); ?>" class="home"> Home </a></span> &gt; <span typeof="v:Breadcrumb"><span property="v:title"><?php the_title(); ?></span></span></div>
				<div class="clearfix"></div>
			</div>
		</div>
        	<div id="main">
		<div class="width-container">
			<div id="full-width-progression">						
				<div id="content-container-full-width">
					<div class="content-container-pro">
						
<div class="ls-sc-grid_6 alpha"><?php the_field('left_content'); ?></div>
<div class="ls-sc-grid_6 omega"><?php the_field('right_content'); ?></div>
						
						<?php
		// check if the repeater field has rows of data
		if( have_rows('section_repeater') ):
				while ( have_rows('section_repeater') ) : the_row();
				
				?>
						<div class="ls-sc-grid_12">
						<div class="ls-sc-grid_6 alpha"><?php the_sub_field('left_section'); ?></div>
<div class="ls-sc-grid_6 omega"><?php the_sub_field('right_section'); ?></div>
						</div>
															<?php
			endwhile;
		else :
		// no rows found
		endif;
		?>	

                    					</div><!-- close .content-container-pro -->
				</div>
				<div class="clearfix"></div>
			</div>
		</div><!-- close .width-container -->
        </div>
 <?php get_footer(); ?>